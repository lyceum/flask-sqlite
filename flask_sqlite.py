""" Une application web python flask avec persistance assurée par sqlite.

"""
import sqlite3

from flask import Flask, g, render_template, request

app = Flask(__name__, template_folder='pages')

DATABASE = 'data.db'


@app.route('/', methods=['GET', 'POST'])
def index_page():
    data = query_db('SELECT * FROM eleve')
    if request.method == 'POST':
        # on récupère les données du formulaire
        post_data = request.form

        # on les ajoute à la relation eleve de la bdd
        try:
            last_row = update_db(
                f'INSERT INTO eleve VALUES (NULL, "{post_data["nom"]}", "{post_data["prenom"]}", "{post_data["classe"]}");')
            print(last_row)
        except Exception as e:
            print("Erreur lors de l'insertion des données dans la db")
            print(e)
        # on reaffiche la page avec la liste mise à jour
        data = query_db('SELECT * FROM eleve')
        return render_template("index.html", data=data)
    else:
        # méthode GET par défaut on affiche
        # la page html index.html avec la liste des élèves
        return render_template("index.html", data=data)


#
# Fonctions relatives à la base de données
#
# Sources:

# https: // flask.palletsprojects.com/en/1.1.x/patterns/sqlite3
# https: //docs.python.org/3.8/library/sqlite3.html#sqlite3.Connection.row_factory

def get_db():
    # from https://flask.palletsprojects.com/en/1.1.x/patterns/sqlite3
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        # Utilisation de dictionnaires au lieu de tuples pour les résultats
        db.row_factory = dict_factory
    return db


def query_db(query, args=(), one=False):
    """Fonction permettant d'exécuter des requêtes de demande SQL"""
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def update_db(query):
    """Fonction permettant d'exécuter des requêtes mise à jour SQL

    Source: https://www.sqlitetutorial.net/sqlite-python/insert/
    """
    db = get_db()
    cur = db.execute(query)
    db.commit()
    return cur.lastrowid


def dict_factory(cursor, row):
    # from https://docs.python.org/3.8/library/sqlite3.html#sqlite3.Connection.row_factory
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


@app.teardown_appcontext
def close_connection(exception):
    # from https://flask.palletsprojects.com/en/1.1.x/patterns/sqlite3
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if __name__ == '__main__':
    app.run(debug=True)
