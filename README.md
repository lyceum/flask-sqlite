# flask_sqlite

Une toute petite application serveur en Python pour montrer comment traiter les
données d'un formulaire `html` côté serveur.

Cette application est destinée à des élèves de **terminale NSI**, les données
sont stockées sous forme d'une base de données sqlite `data.db`.

## Architecture de l'application

- `data_db`: le fichier de base de base de données `sqlite` ne contenant
  initialement qu'une relation `eleve` ayant le schéma relationnel suivant:

   ```sql
   CREATE TABLE "eleve" (
   	    "id"	INTEGER NOT NULL UNIQUE,
   	    "nom"	TEXT,
   	    "prenom"	TEXT,
   	    "classe"	TEXT,
   	    PRIMARY KEY("id" AUTOINCREMENT)
   );
   ```

- `flask_sqlite.py`: l'application serveur python qui permet de servir les pages
  et d'accéder à la base de données. Cette application utilise le package
  [`flask`](https://flask.palletsprojects.com/en/1.1.x/quickstart/) et ne défint
  qu'un seul chemin, la page d'accueil.

  On ne détaille que la méthode `GET` ici, mais une mise à jour de la base de
  données est également implémentée grâce à la méthode `POST`.

  ```python
  @app.route('/')
  def index_page():
      # accés à la base de données
      cur = get_db().cursor()
      # requête SQL 
      data = query_db('SELECT * FROM eleve')
      # méthode GET par défaut on affiche
      # la page html index.html avec la liste des élèves
      return render_template("index.html", data=data)     
  ```

- `pages/index.html`: une page `html` _hydratée_ par les données qui lui sont
  passées par la méthode `render_template("index.html", data=data)`. Le langage
  de _template_ est [`jinja`](https://palletsprojects.com/p/jinja/), il permet
  d'utiliser les variables du dictionnaire `data` pour générer du html
  dynamiquement.

  Par exemple, on utilise une boucle pour afficher tous les eleves de la liste
  `data` récupérée précdemment dans la requête SQL:

  ```jinja
  <tbody>
        {% for eleve in data %}
        <tr>
            <td>{{ eleve["nom"] }}</td>
            <td>{{ eleve["prenom"] }}</td>
            <td>{{ eleve["classe"] }}</td>
        </tr>
        {% endfor %}
    </tbody>
    ```

## Lancer l'application

Pour lancer vous devez avoir une distribution python 3 avec le paquet `flask`
installé(Par défaut sur [anaconda](https://www.anaconda.com/products/individual)
ou [winpython](https://winpython.github.io/)).

L'application peut être lancée avec python: `python flask_sqlite.py`.

D'autres part, des scripts sont fournis pour lancer l'application:

- Sous Windows: `batch run-server.bat`
- Sous Unix bash(Linux, Mac, etc.): `bash run-server.sh`

### Problèmes de lancement

Il se peut qu'en installant python, celui-ci n'ait pas ajouté à la variable système `PATH`.

Si vous avez installé python avec Anaconda ou WINPYTHON sous _Windows_ sans
l'ajouter à la variable système `PATH`, vous devez ajouter le chemin de python au
`PATH` pour lancer la commande `flask`.

Exemple avec Winpython dans mon lycée ou WinPython a été installé dans le
répértoire `C:\Program Files\WPy64-3740\`:

```bat
SET "MY_PATH=C:\Program Files\WPy64-3740\python-3.7.4.amd64\Lib\site-packages\PyQt5;C:\Program Files\WPy64-3740\python-3.7.4.amd64\Lib\site-packages\PySide2;C:\Program Files\WPy64-3740\python-3.7.4.amd64\;C:\Program Files\WPy64-3740\python-3.7.4.amd64\DLLs;C:\Program Files\WPy64-3740\python-3.7.4.amd64\Scripts;C:\Program Files\WPy64-3740\python-3.7.4.amd64\;C:\Program Files\WPy64-3740\python-3.7.4.amd64\..\t\mingw32\bin;C:\Program Files\WPy64-3740\python-3.7.4.amd64\\R\bin\x64;C:\Program Files\WPy64-3740\python-3.7.4.amd64\\Julia\bin;C:\Program Files\WPy6
4-3740\python-3.7.4.amd64\;"
SET "PATH=%MY_PATH%;%PATH%"
```

## Crédits

- [flask](http://flask.pocoo.org/) pour faire tourner l'application serveur.
- [bootstrap](https://getbootstrap.com/) pour la mise en forme css.
